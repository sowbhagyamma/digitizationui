export const useCases = [{"value": "Data Coop", "display": "Data Coop"}, {"value":"ID Sync", "display": "ID Sync"},{"value": "ID Graph", "display": "ID Graph"}, {"value":"Private usage", "display": "Private usage"},{"value": "Client Data Sharing", "display": "Client Data Sharing"}, {"value":"Adara Apps", "display": "Adara Apps"}, {"value":"Market Research", "display": "Market Research"},{"value": "Data Modeling for Data", "display": "Data Modeling for Data"}, {"value":"Products", "display": "Products"}];

export const usageTypes = [{"value": "Advertising", "display": "Advertising"}, {"value":"Analytics", "display": "Analytics"},{"value": "Measurement", "display": "Measurement"}, {"value":"Research", "display": "Research"}];

export const dataSources = [{"value": "Individual Record", "display": "Individual Record"}, {"value":"Aggregate/Derived", "display": "Aggregate/Derived"}];

export const outputPermissions = [{"value": "Hashed Email", "display": "Hashed Email"}, {"value":"Hashed Cookie ID", "display": "Hashed Cookie ID"},{"value": "Hashed Device ID", "display": "Hashed Device ID"}, {"value":"Hashed Custom ID", "display": "Hashed Custom ID"}];

export const appMappings = [{"value": "Search", "display": "Search"}, {"value":"Catalog", "display": "Catalog"},{"value": "On Platform Media", "display": "On Platform Media"}, {"value":"Off Platform Media", "display": "Off Platform Media"}, {"value":"Impact", "display": "Impact"},{"value": "Market Monitor", "display": "Market Monitor"}];