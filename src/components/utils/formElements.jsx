import React from "react";
import { TextField,Box,FormControl,Select,Button,MenuItem } from '@material-ui/core/';
import { DatePicker, MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from "@date-io/date-fns";
import { useStyles } from "../schemaDesign/formStyles";

export function TextFieldElement1({labelname, type, id}){
	const classes = useStyles();
	return(
		<FormControl className={classes.fieldControl}>
			<Box className={classes.labelBox}>{labelname}</Box>
			<TextField required variant="outlined" type={type} id={id} className={classes.textField1}  inputProps={{className: classes.input}}/>
		</FormControl>
	);
}

export function TextFieldElement2({labelname, type, id}){
	const classes = useStyles();
	return(
		<FormControl className={classes.fieldControl}>
			<Box className={classes.labelBox}>{labelname}</Box>
			<TextField required variant="outlined" type={type} id={id} className={classes.textField2}  inputProps={{className: classes.input}}/>
		</FormControl>
	);
}

export function RetentionRuleElements({labelname, type, id1, id2, options, value, onChange}){
	const classes = useStyles();
	return(
		<FormControl className={classes.fieldControl}>
			<Box className={classes.labelBox}>{labelname}</Box>
			<TextField required variant="outlined" type={type} id={id1} className={classes.textField1}  inputProps={{className: classes.input}}/>
			<Select variant="outlined" id={id2} className={classes.selectBox} value={value} onChange={onChange} >
				{options.map((option) => <MenuItem key={option.value} value={option.value}>{option.display}</MenuItem>)}
			</Select>
		</FormControl>
	);
}

export function SelectElement({labelname, id, options, value, onChange}){
	const classes = useStyles();
	return(
		<FormControl className={classes.fieldControl}>
			<Box className={classes.labelBox}>{labelname}</Box>
			<Select variant="outlined" id={id} className={classes.selectBox} value={value} onChange={onChange} >
				{options.map((option) => <MenuItem key={option.value} value={option.value}>{option.display}</MenuItem>)}
			</Select>
		</FormControl>
	);
}

export function MultiSelectElement({labelname, id, options, value, onChange}){	
	const classes = useStyles();
	return(
		<FormControl className={classes.fieldControl}>
			<Box className={classes.labelBox}>{labelname}</Box>
			<Select variant="outlined" id={id}  multiple className={classes.multiSelectBox} value={value} onChange={onChange} native>
				{options.map((option) => <option key={option.value} value={option.value}>{option.display}</option>)}
			</Select>
		</FormControl>
	);
}

export function DatePickerElement({labelname, id, value, onChange}){
	const classes = useStyles();
	
	return (
		<FormControl className={classes.fieldControl}>
			<Box className={classes.labelBox}>{labelname}</Box>		
			<MuiPickersUtilsProvider utils={DateFnsUtils}>
				<DatePicker id={id} format="dd/MM/yyyy" variant="outlined" disablePast onChange={onChange} value={value} className={classes.textField1}  inputProps={{className: classes.input}}/>
			</MuiPickersUtilsProvider>
		</FormControl>
	)
}

export function SubmitButton({label}){
	const classes = useStyles();
	return(
		<div className={classes.submitButton}>
			<Button type="submit" variant="contained" color="primary" >{label}</Button>	
		</div>
	);
}