export const idTypes = [{"value": "Not an ID", "display": "Not an ID"}, {"value":"Email", "display": "Email"},{"value": "Cookie ID", "display": "Cookie ID"}, {"value":"Device ID", "display": "Device ID"},{"value": "Custom ID", "display": "Custom ID"}, {"value":"3rd Party Graph ID", "display": "3rd Party Graph ID"}];

export const encryptionTypes = [{"value": "SHA1", "display": "SHA1"}, {"value":"SHA256", "display": "SHA256"},{"value": "MD5", "display": "MD5"}, {"value":"None", "display": "None"}];

export const dataClassifications = [{"value": "Limited", "display": "Limited Access"}, {"value":"Protected", "display": "Protected"}];