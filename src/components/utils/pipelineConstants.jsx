export const pipelineTypes = [{"value": "PS", "display": "PS"}, {"value":"NPS", "display": "NPS"}];

export const dataCategories = [{"value": "1st Part", "display": "1st Part"}, {"value":"3rd Part", "display": "3rd Part"}];

export const dataTypes = [{"value": "CRM", "display": "CRM"}, {"value":"Audience", "display": "Audience"},{"value": "Behavior", "display": "Behavior"}, {"value":"Transaction", "display": "Transaction (Search/Booking/Purchase)"},{"value": "Other Transactiosn", "display": "Other Transactiosn"}, {"value":"ID Match", "display": "ID Match"}];

export const classifications = [{"value": "Limited", "display": "Limited Access"}, {"value":"Protected", "display": "Protected"}];

export const removalProcess = [{"value": "De-activate", "display": "De-activate"}, {"value":"Delete", "display": "Delete"}, {"value":"Anonymize", "display": "Anonymize"}];

export const regulatoryPurposes = [{"value": "Advertising", "display": "Advertising"}, {"value":"Analytics", "display": "Analytics"},{"value": "Measurement", "display": "Measurement"}, {"value":"Research", "display": "Research"}];

export const storageLocations = [{"value": "amer1", "display": "amer1"}, {"value":"amer2", "display": "amer2"},{"value": "apac1", "display": "apac1"}, {"value":"apac2", "display": "apac2"},{"value": "emea1", "display": "emea1"}, {"value":"default", "display": "default"}];

export const useCases = [{"value": "Data Coop", "display": "Data Coop"}, {"value":"ID Sync", "display": "ID Sync"},{"value": "ID Graph", "display": "ID Graph"}, {"value":"Private usage", "display": "Private usage"},{"value": "Client Data Sharing", "display": "Client Data Sharing"}, {"value":"Adara Apps", "display": "Adara Apps"}, {"value":"Market Research", "display": "Market Research"},{"value": "Data Modeling for Data", "display": "Data Modeling for Data"}, {"value":"Products", "display": "Products"}];

export const dataSharings = [{"value": "Client1", "display": "Client1"}, {"value":"Client2", "display":"Client2"}, {"value":"Client3", "display": "Client3"}];

export const destPlatforms = [{"value": "Dest1", "display": "Dest1"}, {"value":"Dest2", "display":"Dest2"}, {"value":"Dest3", "display": "Dest3"}];

export const ingestionServices = [{"value": "Adara Pixel", "display": "Adara Pixel"}, {"value":"First Party Pixel", "display":"First Party Pixel"}, {"value":"Onboarding API", "display": "Onboarding API"},{"value": "Onboarding API offline", "display": "Onboarding API offline"}, {"value":"Batch", "display":"Batch"}, {"value":"Modata API", "display": "Modata API"},{"value": "Manual BQ ETL", "display": "Manual BQ ETL"}, {"value":"LiveRamp Inbound", "display":"LiveRamp Inbound"}, {"value":"Oracle Inbound", "display": "Oracle Inbound"},{"value": "Adobe Audience Manager", "display": "Adobe Audience Manager"}, {"value":"Salesforce integration", "display":"Salesforce integration"}];

export const frequencyTypes = [{"value": "Real Time", "display": "Real Time"}, {"value":"Daily/Weekly/Monthly", "display":"Daily/Weekly/Monthly"}, {"value":"Ad Hoc", "display": "Ad Hoc"}];

export const channels = [{"value": "Web", "display": "Web"}, {"value":"Mobile Web", "display": "Mobile Web"},{"value": "Mobile app", "display": "Mobile app"}, {"value":"Offline", "display": "Offline"}];

export const dataSources = [{"value": "Source1", "display": "Source1"}, {"value":"Source2", "display": "Source2"}];

export const dataEncryptionTypes = [{"value": "Default", "display": "Default"}];

export const retentionPeriod = [{"value": "day", "display": "day"},{"value": "week", "display": "week"},{"value": "month", "display": "month"}];