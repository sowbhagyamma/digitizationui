import React from "react";
import { Typography } from '@material-ui/core/';
import { useStyles } from "./formStyles";
import * as specificatinsconstants from "../utils/specificationsConstants";
import { TextFieldElement1, SelectElement, SubmitButton } from "../utils/formElements";

export function Specification() {
	const classes = useStyles();
	function submitSpecificationForm(){
		const data = {
			"description":document.getElementById("elementdescription").value,
			"idType":idType,
			"encryption":encryption,
			"classification":classification
		}
		alert("Data: "+  JSON.stringify(data));		
	}
	const [ idType, setIdType ] = React.useState('');
	const handleIdTypeChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setIdType(event.target.value);	
	};
	
	const [ encryption, setEncryption ] = React.useState('');
	const handleEncryptionChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setEncryption(event.target.value);	
	};
	
	const [ classification, setClassification ] = React.useState('');
	const handleClassificationChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setClassification(event.target.value);	
	};
	return(
		<div className={classes.formLayout} >
			<Typography variant="h4" gutterBottom>
				Data Specification and Classification
			</Typography>
			<form id="specificationform" onSubmit={submitSpecificationForm}>
				<TextFieldElement1 labelname="Element Description:" type="text" id="elementdescription" />
				<SelectElement labelname="ID Type:" id="idtype" value={idType} options={specificatinsconstants.idTypes} onChange={handleIdTypeChange} />	
				<SelectElement labelname="Encryption:" id="encryption" value={encryption} options={specificatinsconstants.encryptionTypes} onChange={handleEncryptionChange}/>	
				<SelectElement labelname="Data Classification:" id="dataclassification" value={classification} options={specificatinsconstants.dataClassifications} onChange={handleClassificationChange} />	
				<SubmitButton label="Submit" />
			</form>
		</div>
	);
}