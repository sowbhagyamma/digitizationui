import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  fieldControl: {
	flexDirection: 'row',
	width:"100%",
	margin: "3px 0px 3px 0px"
  },  
  textField1 : {
	 width:"25%" ,		 
  },
  textField2 : {
	 width:"75%" ,	 
  },
  labelBox : {
	  width:"15%",
	  display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
	  fontWeight: "bold"
  },
  selectBox : {	  
	  width: "25%",	
	  height: 30
  },
 multiSelectBox : {	  
	  width: "25%",	
  },
  formLayout : {
	margin:"10px 0px 0px 20%"
  },
  input : {
	 padding:5 
  },
  submitButton : {
	  padding : "10px 35%"
  }
}));