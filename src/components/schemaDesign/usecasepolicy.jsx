import React from "react";
import { Typography } from '@material-ui/core/';
import { useStyles } from "./formStyles";
import * as usecaseconstants from "../utils/useCasePolicyConstants";
import { SelectElement, MultiSelectElement, SubmitButton } from "../utils/formElements";


export function UseCasePolicy(){
	const classes = useStyles();
	function submitPipelineForm(){
		const data = {
			"useCase":useCase,
			"usageType":usageType,
			"outputPermission":outputPermission,
			"dataSourcing":dataSourcing,
			"appMapping":appMapping
		};
		alert("Data:"+JSON.stringify(data));		
	}
	const [useCase, setUseCase] = React.useState([]);
	const handleUseCaseChange = (event: React.ChangeEvent<{ value: unknown }>) => {		
		setUseCase(event.target.value);
	  };
	  
	const [usageType, setUsageType] = React.useState([]);
	const handleUsageTypeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setUsageType(value);
	  };
	const [outputPermission, setOutputPermission] = React.useState([]);
	const handleOutputPermissionChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setOutputPermission(value);
	  };
	const [ dataSourcing, setDataSourcing ] = React.useState('');
	const handleDataSourcingChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setDataSourcing(event.target.value);	
	};
	const [ appMapping, setAppMapping ] = React.useState('');
	const handleAppMappingChange = (event: React.ChangeEvent<{ value: unknown }>) => {	
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setAppMapping(value);	
	};
	return(
		<div className={classes.formLayout} >
			<Typography variant="h4" gutterBottom>
				Use Case Policy
			</Typography>
			<form id="usecasepolicyform" onSubmit={submitPipelineForm}>	
				<SelectElement labelname="Use Case:" id="usecase" options={usecaseconstants.useCases} value={useCase} onChange={handleUseCaseChange} />	
				<MultiSelectElement labelname="Usage Type:" id="usagetype" options={usecaseconstants.usageTypes} value={usageType} onChange={handleUsageTypeChange} />
				<SelectElement labelname="Data Sourcing:" id="datasourcing" options={usecaseconstants.dataSources} value={dataSourcing} onChange={handleDataSourcingChange} />	
				<MultiSelectElement labelname="Output Permission:" id="outputpermission" options={usecaseconstants.outputPermissions} value={outputPermission} onChange={handleOutputPermissionChange} />		
				<MultiSelectElement labelname="Use Case-App Mapping:" id="appmapping" options={usecaseconstants.appMappings} value={appMapping} onChange={handleAppMappingChange} />	
				
				<SubmitButton label="Submit" />			
			</form>
		</div>
	);
}