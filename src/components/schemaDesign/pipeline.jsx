import React from "react";
import { Typography } from '@material-ui/core/';
import { useStyles } from "./formStyles";
import * as pipelineconstants from "../utils/pipelineConstants";
import { TextFieldElement1, TextFieldElement2, SelectElement, MultiSelectElement, SubmitButton, DatePickerElement,RetentionRuleElements} from "../utils/formElements";

export function PipeLine() {	
	const classes = useStyles();
	
	function submitPipelineForm(){
		const data = {
			"providerId":document.getElementById("providerid").value,
			"pipeline":pipeLine,
			"Version":document.getElementById("version").value,
			"msg_delimiter":document.getElementById("msg_delimiter").value,
			"msg_layout":document.getElementById("msg_layout").value,
			"dataCategory":dataCategory,
			"dataType":dataType,
			"classification":classification,
			"dataRemoval":dataRemoval,
			"retentionrule":document.getElementById("retentionrule").value,
			"regulatoryPurpose":regulatoryPurpose,
			"storageLocation":storageLocation,
			"useCase":useCase,
			"dataSharing":dataSharing,
			"destinationPlatform":destinationPlatform,
			"dataService":dataService,
			"frequency":frequency,
			"channel":channel,
			"dataSource":dataSource,
			"dataEncryption":dataEncryption,
			"startDate":document.getElementById("startDate").value,
			"endDate":document.getElementById("endDate").value
		}
		alert("Data: "+  JSON.stringify(data));		
	}
	const [dataCategory, setDataCategory] = React.useState([]);	
	const handleCategoryChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		console.log("new value", value);
		setDataCategory(value);
	  };
	const [dataType, setDataType] = React.useState([]);
	const handleTypeChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setDataType(value);
	  };
	const [regulatoryPurpose, setRegulatoryPurpose] = React.useState([]);
	const handleRegulatoryChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setRegulatoryPurpose(value);
	  };	  
	const [useCase, setUseCase] = React.useState([]);
	const handleuseCaseChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setUseCase(value);
	  };
	const [dataSharing, setDataSharing] = React.useState([]);
	const handleDataSharingChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setDataSharing(value);
	  };
	const [channel, setChannel] = React.useState([]);	
	const handleChannelChange = (event: React.ChangeEvent<{ value: unknown }>) => {
		const { options } = event.target;
		const value: string[] = [];
		for (let i = 0, l = options.length; i < l; i += 1) {
		  if (options[i].selected) {
			value.push(options[i].value);
		  }
		}
		setChannel(value);
	  };
	  
	const [ pipeLine, setPipeLine ] = React.useState('');
	const handlePipelineChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setPipeLine(event.target.value);	
	};	
	const [ classification, setClassification ] = React.useState('');
	const handleClassificationChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setClassification(event.target.value);	
	};
	const [ dataRemoval, setDataRemoval ] = React.useState('');
	const handleDataRemovalChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setDataRemoval(event.target.value);	
	};	
	const [ storageLocation, setStorageLocation ] = React.useState('');
	const handleStorageLocationChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setStorageLocation(event.target.value);	
	};	
	const [ destinationPlatform, setDestinationPlatform ] = React.useState('');
	const handleDestinationPlatformChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setDestinationPlatform(event.target.value);	
	};
	const [ dataService, setDataService ] = React.useState('');
	const handleDataServiceChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setDataService(event.target.value);	
	};
	const [ frequency, setFrequency ] = React.useState('');
	const handleFrequencyChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setFrequency(event.target.value);	
	};
	const [ dataSource, setDataSource ] = React.useState('');
	const handledataSourceChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setDataSource(event.target.value);	
	};
	const [ dataEncryption, setDataEncryption ] = React.useState('');
	const handleDataEncryptionChange = (event: React.ChangeEvent<{ value: unknown }>) => {			
		setDataEncryption(event.target.value);	
	};
	
	const [ startDate, setStartDate ] = React.useState(new Date());
	function handleStartDateChange(date){			
		setStartDate(date);
	};
	
	const [ endDate, setEndDate ] = React.useState(new Date());
	function handleEndDateChange(date){			
		setEndDate(date);
	};
	
	const [ retentionPeriod, setRetentionPeriod ] = React.useState('');
	const handleRetentionPeriodChange = (event: React.ChangeEvent<{ value: unknown }>) => {						
		setRetentionPeriod(event.target.value);
	};

  return (  
	<div className={classes.formLayout} >
		<Typography variant="h4" gutterBottom>
			Data PipeLine and Policy Management
		</Typography>
		<form id="pipelineform" onSubmit={submitPipelineForm}>
			<TextFieldElement1 labelname="Data Provider Id:" type="text" id="providerid" />
			<SelectElement labelname="PipeLine:" id="pipeline" options={pipelineconstants.pipelineTypes} value={pipeLine} onChange={handlePipelineChange}/>	
			<TextFieldElement1 labelname="Version:" type="number" id="version" />	
			<TextFieldElement1 labelname="Message Delimiter:" type="text" id="msg_delimiter" />
			<TextFieldElement2 labelname="Message Layout:" type="text" id="msg_layout" />
			<MultiSelectElement labelname="DataCategory:" id="datacategory" options={pipelineconstants.dataCategories} value={dataCategory} onChange={handleCategoryChange} />
			<MultiSelectElement labelname="Type of Data:" id="datatype" options={pipelineconstants.dataTypes} value={dataType} onChange={handleTypeChange} />	
			<SelectElement labelname="Data Classification:" id="dataClassification" options={pipelineconstants.classifications} value={classification} onChange={handleClassificationChange}/>	
			<SelectElement labelname="Data Removal Processing:" id="dataRemoval" options={pipelineconstants.removalProcess} value={dataRemoval} onChange={handleDataRemovalChange}/>	
			<RetentionRuleElements labelname="Data Retention Rule:" type="number" id1="retentionrule" id2="retentionperiod" options={pipelineconstants.retentionPeriod} value={retentionPeriod} onChange={handleRetentionPeriodChange}/>
			<MultiSelectElement labelname="Regulatory Purpose:" id="regulatorypurpose" options={pipelineconstants.regulatoryPurposes} value={regulatoryPurpose} onChange={handleRegulatoryChange} />		
			<SelectElement labelname="Storage Location:" id="storagelocation" options={pipelineconstants.storageLocations} value={storageLocation} onChange={handleStorageLocationChange} />				
			<MultiSelectElement labelname="Use Case:" id="usecase" options={pipelineconstants.useCases} value={useCase} onChange={handleuseCaseChange} />
			<MultiSelectElement labelname="Data Sharing:" id="datasharing" options={pipelineconstants.dataSharings} value={dataSharing} onChange={handleDataSharingChange} />
			<SelectElement labelname="Destination Platform:" id="destinationplatform" options={pipelineconstants.destPlatforms} value={destinationPlatform} onChange={handleDestinationPlatformChange}/>				
			<SelectElement labelname="Ingestion Data Service:" id="dataservice" options={pipelineconstants.ingestionServices} value={dataService} onChange={handleDataServiceChange}/>
			<SelectElement labelname="Frequency:" id="frequency" options={pipelineconstants.frequencyTypes} value={frequency} onChange={handleFrequencyChange}/>	
			<MultiSelectElement labelname="Channel:" id="channel" options={pipelineconstants.channels} value={channel} onChange={handleChannelChange} />
			<SelectElement labelname="Data Source:" id="datasource" options={pipelineconstants.dataSources} value={dataSource} onChange={handledataSourceChange}/>	
			<SelectElement labelname="Data Encryption:" id="dataencryption" options={pipelineconstants.dataEncryptionTypes} value={dataEncryption} onChange={handleDataEncryptionChange}/>
			<DatePickerElement labelname="Start Date:" id="startdate" disablePast onChange={handleStartDateChange} value={startDate} />					
			<DatePickerElement labelname="End Date:" id="enddate" disablePast onChange={handleEndDateChange} value={endDate} />
			<SubmitButton label="Submit" />			
		</form>
	</div>
  );
}