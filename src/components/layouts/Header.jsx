import React, { Fragment } from "react";
import clsx from "clsx";
import { AppBar, Tabs, Tab } from "@material-ui/core";
import { useStyles } from "./layoutStyles";
import { Switch, Route, Link, BrowserRouter } from "react-router-dom";
import { PipeLine } from "../schemaDesign/pipeline.jsx";
import { Specification } from "../schemaDesign/specification.jsx";
import { UseCasePolicy } from "../schemaDesign/usecasepolicy.jsx";

export function Header() {
  const classes = useStyles();

  return (
  <BrowserRouter>
    <AppBar position="sticky" className={clsx(classes.appBar, { [classes.appBarShift]: false })} >
      <Route
          path="/"
          render={({ location }) => (
            <Fragment>
              <Tabs value={location.pathname}>
                <Tab className={classes.tabLabel} label="Data PipeLine and Policy Management" component={Link} to="/pipeline" />
                <Tab className={classes.tabLabel} label="Data Specification and Classification" component={Link} to="/specification" />
                <Tab className={classes.tabLabel} label="Use Case Policy" component={Link} to="/usecasepolicy" />
              </Tabs>             
            </Fragment>
          )}
        />
    </AppBar>
	 <Switch>
		<Route path="/pipeline" render={() => <PipeLine />} />
		<Route path="/specification" render={() => <Specification />} />
		<Route path="/usecasepolicy" render={() => <UseCasePolicy />} />		
	  </Switch>
	</BrowserRouter>	
  );
}