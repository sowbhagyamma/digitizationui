import React from 'react';
import { CssBaseline } from "@material-ui/core";
import { Header } from "./components/layouts/Header.jsx";

function App() {
  return (
    <div>
	  <CssBaseline />
	  <Header />	  
	</div>
  );
}

export default App;
